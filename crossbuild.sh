# docker run --rm --privileged multiarch/qemu-user-static:register --reset
docker run --rm --privileged multiarch/qemu-user-static --reset -p yes
docker buildx build \
	-f Dockerfile-crossbuild \
	--push \
	--platform linux/arm64/v8 \
	--tag listerine/arm64v8-py3.7-opencv:latest .
# docker build -t opencv-arm-multistage:4.0.0-py3.5 -f Dockerfile-crossbuild .
